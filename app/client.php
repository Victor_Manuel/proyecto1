<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
       protected $fillable = [
        'name',
        'lastName',
        'secoundLastName',
        'birthday',
        'sex',
        'telephone',
        'email',
        'address',
        'postalCode',
        'city',
        'state',
    ];
}
