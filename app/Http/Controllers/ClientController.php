<?php

namespace App\Http\Controllers;

use App\client;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
   
    //comprobar si la solicitud Ajax
    if($request->ajax())
    {      
      //permiten ordenar fácilmente los resultados por fecha.
        $data = client::latest()->get();
      // envia los datos a datatables y añade  los butones 
        return DataTables::of($data)
                ->addColumn('action', function($data){
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Editar</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Eliminar</a>';
                            return $btn;
                   
                })
                ->rawColumns(['action'])
                ->make(true);
    }
       

        return view('client');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $Client =client::updateOrCreate(['id' => $request->Item_id],
                            ['name' => $request->name, 
                            'lastName' => $request->lastName,
                            'secoundLastName' => $request->secoundLastName,
                            'birthday' => $request->birthday,
                            'sex' => $request->sex,
                            'telephone' => $request->telephone,
                            'email' => $request->email,
                            'address' => $request->address,
                            'postalCode' => $request->postalCode,
                            'city' => $request->city,
                            'state' => $request->state ]);        
        return response()->json(['success'=>'Guardado exitosamente.']);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     * client $client
     */
    public function edit($id)
    {
        
        $Client = client::find($id);
        return response()->json($Client);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, client $client)
    {

  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\client  $client
     * @return \Illuminate\Http\Response
     * client $client
     */

    public function destroy($id)
    {
        //eliminamos registro
       client::find($id)->delete();
         //retornamos una respuest
       return response()->json(['success'=>'borrado exitosamente']);
    }
}
