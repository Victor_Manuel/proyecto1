<!DOCTYPE html>
<html>
<head>
  <title>Laravel 7 Ajax CRUD tutorial using Datatable</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/main_style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet">
 
  
  <link href="https://cdn.datatables.net/1.10.17/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
  <script src="https://cdn.datatables.net/1.10.17/js/jquery.dataTables.min.js"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
   <link href="{{ asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet">
   <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('js/responsive.bootstrap.min.js') }}"></script>
  

  <main class="container well">
  </head>
  <body class="bg-dark">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="card ">
           <div class="card-header">
            <div class="col-12">
              <h4 class="card-title">Laravel 7  usando datatable
                <a class="btn btn-success ml-5" href="javascript:void(0)" id="createNewItem"> Nuevo cliente</a>
              </h4>
            </div>
          </div>

          <table class="table table-bordered data-table  table-responsive " style="width: 100%" >
            <thead>
              <tr>
                <th width="5%">No</th>
                <th>Nombre</th>
                <th>Primer Apellido</th>
                <th>Segundo Apellido</th>
                <th>Fecha de Nacimiento</th>
                <th>Género</th>
                <th>Teléfono</th>
                <th>Email</th>
                <th>Dirección</th>
                <th>Código Postal</th>
                <th>Ciudad</th>
                <th>Estado</th>
                <th width="15%">Acciones</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="modal fade" id="ajaxModel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header ">
                <h4 class="modal-title" id="modelHeading"></h4>
              </div>
              <div class="modal-body">
                <form id="ItemForm" name="ItemForm" class="form-horizontal">
                 <input type="hidden" name="Item_id" id="Item_id">

                 <div class="row">
                  <div class="col-md-12">
                    <div class="form-group col-md-6 mb-2">
                      <label>Nombre:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="128" id="name" name="name"  type="text" placeholder="Nombre">
                      </div>                
                    </div>
                    <div class="form-group col-md-6 ">
                      <label>Primer apellido:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="11" id="lastName" name="lastName" type="text" placeholder="Primer apellido" >
                      </div>
                    </div>
                  </div>
                </div>      
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group col-md-6">
                      <label>Segundo apellido:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="128" id="secoundLastName" name="secoundLastName"  type="text" placeholder="Segundo apellido">
                      </div>                
                    </div>
                    <div class="form-group col-md-6">
                      <label>Fecha de nacimiento:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="11" id="birthday" name="birthday" type="date" placeholder="Fecha de nacimiento" >
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group col-md-6">
                      <label>Género:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="128"id="sex" name="sex"  type="text" placeholder="Género">
                      </div>                
                    </div>
                    <div class="form-group col-md-6">
                      <label>Teléfono</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="11" id="telephone" name="telephone" type="text" placeholder="Teléfono" >
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group col-md-6">
                      <label>Email:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="128"id="email" name="email"  type="text" placeholder="Email">
                      </div>                
                    </div>
                    <div class="form-group col-md-6">
                      <label>Dirección</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="11" id="address" name="address"type="text" placeholder="Dirección" >
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group col-md-6">
                      <label>Código Postal:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="128" id="postalCode" name="postalCode" placeholder="Código Postal"   type="text">
                      </div>                
                    </div>
                    <div class="form-group col-md-6">
                      <label>Ciudad</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="11" id="city" name="city"type="text" >
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group col-md-6">
                      <label>Estado:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input class="form-control" maxlength="128"id="state" name="state"placeholder="Estado"   type="text">
                      </div>                
                    </div>

                    <div class="form-group col-md-1">
                      <label>&nbsp;</label>
                      <button type="submit" class="btn btn-primary" id="saveBtn" value="create">
                        <i class="fa fa-plus"> </i>
                        Guardar
                      </button>

                    </div>
                  </div>
                </div>               
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script type="text/javascript">
  window.base_url = '{{ url('/') }}'
</script>


<script type="text/javascript">


  $(function () {

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
        // inicializando Datatable
        var table = $('.data-table').DataTable({
          responsive: true,
          processing: true,
          serverSide: true,
          ajax: "{{ route('client.index') }}",
          columns: [
          { data: 'id', name: 'id'},
          { data: 'name', name: 'name'},
          {data: 'lastName', name: 'Apellido Paterno'},
          {data: 'secoundLastName', name: 'Ape'},
          {data: 'birthday', name: 'last_name'},
          {data: 'sex', name: 'last_name'},
          {data: 'telephone', name: 'last_name'},
          {data: 'email', name: 'last_name'},
          {data: 'address', name: 'last_name'},
          { data: 'postalCode', name: 'last_name'},
          {data: 'city', name: 'last_name'},
          {data: 'state', name: 'last_name'},
          {data: 'action', name: 'action', orderable: false, searchable: false},
          ],

          language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
            "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          },
        });
     //crear un nuevo cliente 
     $('#createNewItem').click(function () {
      $('#saveBtn').val("create-Item");
      $('#Item_id').val('');
      $('#ItemForm').trigger("reset");
      $('#modelHeading').html("Nuevo Cliente");
      $('#ajaxModel').modal('show');
    });
    //editar cliente
    $('body').on('click', '.editItem', function () {
      var Item_id = $(this).data('id');

      $.get("{{ route('client.index') }}" +'/' + Item_id +'/edit', function (data) {
        $('#modelHeading').html("Editar cliente");
        $('#saveBtn').val("edit-user");
        $('#ajaxModel').modal('show');
        $('#Item_id').val(data.id);
        $('#name').val(data.name);
        $('#lastName').val(data.lastName);
        $('#secoundLastName').val(data.secoundLastName);
        $('#birthday').val(data.birthday);
        $('#sex').val(data.sex);
        $('#telephone').val(data.telephone);
        $('#email').val(data.email);
        $('#address').val(data.address);
        $('#postalCode').val(data.postalCode);
        $('#city').val(data.city);
        $('#state').val(data.state);

      })
    });
    //guardar cliente 
    $('#saveBtn').click(function (e) {
      e.preventDefault();
      $(this).html('Sending..');

      $.ajax({
        data: $('#ItemForm').serialize(),
        url: "{{ route('client.store') }}",
        type: "POST",
        dataType: 'json',
        success: function (data) {

          $('#ItemForm').trigger("reset");
          $('#ajaxModel').modal('hide');
          table.draw();


        },
        error: function (data) {
          console.log('Error:', data);

          $('#saveBtn').html('Save Changes');
        }
      });
    });
    


    //eliminar cliente
    $('body').on('click', '.deleteItem', function () {

     var Item_id = $(this).data("id");
     var swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

     swalWithBootstrapButtons.fire({
      title: '¿Estás seguro?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Sí, elimínelo!',
      cancelButtonText: '¡No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: "{{ route('client.store') }}"+'/'+Item_id,
          type: 'DELETE',
          dataType: 'JSON',
          success: function (data) {
            if (data.error) {
              $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
              $("#alertMsgHome").html(data.msg);
              setTimeout(function () {
                $("#alertHome").addClass("display-none").removeClass("alert-success")
              }, 3000);
            } else {
              swalWithBootstrapButtons.fire(
                '¡Eliminado!',
                'Su archivo ha sido eliminado.',
                'success'
                )
              table.draw();
            }
          }
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
        ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'Tu archivo imaginario está seguro :)',
          'error'
          )
      }
    })


  });

  });
</script>
</html>