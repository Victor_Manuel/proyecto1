
  $(function () {
     
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
        // inicializando Datatable
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        url:window.app + "client",
        columns: [
        { data: 'id', name: 'id'},
        { data: 'name', name: 'name'},
        {data: 'lastName', name: 'Apellido Paterno'},
        {data: 'secoundLastName', name: 'Ape'},
        {data: 'birthday', name: 'last_name'},
        {data: 'sex', name: 'last_name'},
        {data: 'telephone', name: 'last_name'},
        {data: 'email', name: 'last_name'},
        {data: 'address', name: 'last_name'},
        { data: 'postalCode', name: 'last_name'},
        {data: 'city', name: 'last_name'},
        {data: 'state', name: 'last_name'},
        {data: 'action', name: 'action', orderable: false, searchable: false},
        ],

         language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
    });
     //crear un nuevo cliente 
    $('#createNewItem').click(function () {
        $('#saveBtn').val("create-Item");
        $('#Item_id').val('');
        $('#ItemForm').trigger("reset");
        $('#modelHeading').html("Nuevo Cliente");
        $('#ajaxModel').modal('show');
    });
    //editar cliente
    $('body').on('click', '.editItem', function () {
      var Item_id = $(this).data('id');
     
      $.get("{{ route('client.index') }}" +'/' + Item_id +'/edit', function (data) {
          $('#modelHeading').html("Editar cliente");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#Item_id').val(data.id);
          $('#name').val(data.name);
          $('#lastName').val(data.lastName);
          $('#secoundLastName').val(data.secoundLastName);
          $('#birthday').val(data.birthday);
          $('#sex').val(data.sex);
          $('#telephone').val(data.telephone);
          $('#email').val(data.email);
          $('#address').val(data.address);
          $('#postalCode').val(data.postalCode);
          $('#city').val(data.city);
          $('#state').val(data.state);
          
      })
   });
    //guardar cliente 
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          data: $('#ItemForm').serialize(),
          url: "{{ route('client.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
     
              $('#ItemForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();
           
         
          },
          error: function (data) {
              console.log('Error:', data);

              $('#saveBtn').html('Save Changes');
          }
      });
    });
    


    //eliminar cliente
    $('body').on('click', '.deleteItem', function () {

     var Item_id = $(this).data("id");
      var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: '¿Estás seguro?',
        text: "¡No podrás revertir esto!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '¡Sí, elimínelo!',
        cancelButtonText: '¡No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
          url: "{{ route('client.store') }}"+'/'+Item_id,
            type: 'DELETE',
            dataType: 'JSON',
            success: function (data) {
              if (data.error) {
                $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                $("#alertMsgHome").html(data.msg);
                setTimeout(function () {
                  $("#alertHome").addClass("display-none").removeClass("alert-success")
                }, 3000);
              } else {
                swalWithBootstrapButtons.fire(
                  '¡Eliminado!',
                  'Su archivo ha sido eliminado.',
                  'success'
                  )
                   table.draw();
              }
            }
          });
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
          ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'Tu archivo imaginario está seguro :)',
            'error'
            )
        }
      })


    });
     
  });